"""PPW_kun URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from Lab4.views import beranda as page0
from Lab4.views import home as page1
from Lab4.views import profil as page2
from Lab4.views import portfolio as page3
from Lab4.views import jadwalSub as page5
from Lab4.views import dataJadwal as page6
from Lab4.views import bikinJadwal as page7
from Lab4.views import hapusSemua as page8
from Lab4.views import guest as page4
from django.urls import path, include
from django.urls import re_path,include


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('Lab6/', include('Lab6.urls')),
    re_path('Lab8/',include('Lab8.urls')),
    re_path('Lab9/', include('Lab9.urls')),
    re_path('Lab10/', include('Lab10.urls')),
    re_path('dataJadwal', page6, name="dataJadwal"),
    re_path('jadwalSub',page5, name="jadwalSub"),   
    re_path('bikinJadwal', page7, name="bikinJadwal"),
    re_path('hapusSemua', page8, name="hapusSemua"),
    re_path(r'^$', page0,name="beranda"),
    re_path('home', page1,name="home"),
    re_path('profil', page2, name="profil"),
    re_path('portfolio', page3, name="portfolio"),
    re_path('guest', page4, name="guest")

]
