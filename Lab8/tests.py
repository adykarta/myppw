from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class story8Test(TestCase):
	def test_Lab8_url_is_exist(self):
		response = Client().get('/Lab8/')
		self.assertEqual(response.status_code,200)

	def test_Lab8_using_home_template(self):
		response = Client().get('/Lab8/')
		self.assertTemplateUsed(response,'home.html')

	def test_Lab8_using_home_func(self):
		found = resolve('/Lab8/')
		self.assertEqual(found.func,home)


# class NewVisitorTest(unittest.TestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		service_log_path = "./chromedriver.log"
# 		service_args = ['--verbose']
# 		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		self.browser.implicitly_wait(3) 
# 		super(NewVisitorTest,self).setUp()

# 	def tearDown(self):
# 		self.browser.implicitly_wait(3)
# 		self.browser.quit()

# 	def test_body_color_and_font(self):
# 		self.browser.get('http://kartadibrata.herokuapp.com/Lab8/')
# 		time.sleep(3)
# 		bodycolor = self.browser.find_element_by_id('bg1').value_of_css_property('background-color')
# 		fontcolor = self.browser.find_element_by_id('bg1').value_of_css_property('color')
# 		self.assertEqual(bodycolor,'rgba(0, 0, 0, 1)')
# 		self.assertEqual(fontcolor,'rgba(255, 255, 255, 1)')

# 	def test_body_color_and_font_when_clicked(self):
# 		self.browser.get('http://kartadibrata.herokuapp.com/Lab8/')
# 		ganti_tema=self.browser.find_element_by_tag_name('button')
# 		ganti_tema.click()
# 		time.sleep(3)
# 		bodycolor = self.browser.find_element_by_id('bg1').value_of_css_property('background-color')
# 		fontcolor = self.browser.find_element_by_id('bg1').value_of_css_property('color')
# 		self.assertEqual(bodycolor,'rgba(211, 211, 211, 1)')
# 		self.assertEqual(fontcolor,'rgba(0, 0, 0, 1)')


	

	
