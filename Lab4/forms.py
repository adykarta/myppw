from django import forms

class SchedForm(forms.Form):

	OPTION=(
		('Akademis', 'Akademis'),
		('Non Akademis', 'Non Akademis'),
		('Lainnya', 'Lainnya'),
		)
	nama= forms.CharField(label='Nama Kegiatan')
	day= forms.CharField(label='Hari')
	date= forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
	time= forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
	tempat= forms.CharField(label='Tempat')
	kategori=forms.ChoiceField(label='Kategori', choices=OPTION)

	


