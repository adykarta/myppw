from django.shortcuts import render, redirect
from .models import Sched
from .forms import SchedForm
# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
response={}

def beranda(request):
	return render(request,'beranda.html')

def home(request):
		
	return render(request,'Story 3.html')

def profil(request):
		
	return render(request,'Profil.html')

def portfolio(request):
		
	return render(request,'Portfolio.html')

def guest(request):
		
	return render(request,'GuestBook.html')

def jadwalSub(request):
	response['form']= SchedForm
	return render(request,'Schedule.html', response)

def bikinJadwal(request):
	form =SchedForm(request.POST or None)	
	if(request.method =="POST"):
		response["nama"] = request.POST["nama"]
		response["day"] = request.POST["day"]
		response["date"] = request.POST["date"]
		response["time"] = request.POST["time"]
		response["tempat"] = request.POST["tempat"]
		response["kategori"] = request.POST["kategori"]
		jadwal_pribadi = Sched(nama=response["nama"], day=response["day"], date=response["date"], time=response["time"],
			tempat=response["tempat"], kategori=response["kategori"])
		jadwal_pribadi.save()
		return HttpResponseRedirect('dataJadwal')
		
	else:
		return HttpResponseRedirect('/')


def dataJadwal(request):
	response["list_jadwal"] = Sched.objects.all().values()
	return render(request,'DataJadwal.html', response)

def hapusSemua(request):
	 hapusAll= Sched.objects.filter().delete()
	 return HttpResponseRedirect('dataJadwal')


