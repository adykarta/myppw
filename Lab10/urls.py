from django.conf.urls import url
from Lab10 import views as Lab10

urlpatterns = [
	url(r'^$', Lab10.profile),
	url('subscriber',Lab10.addSubscriber,name = "subscriber"),
	url('validate_email',Lab10.validate_email,name = "validate_email"),
	url('hapusSub', Lab10.unsub,name = "unsub"),
	url('registrasi',Lab10.regist),
	# url('subscriber',Lab10.addSubscriber,name="subscriber")
]
