from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import subs
from.models import Subscriber
from django.views.decorators.csrf import csrf_exempt
import json
import re


# Create your views here.
def profile(request):
	return render(request,'profile.html')

def regist(request):
	form = subs(request.POST or None)
	return render(request,"registrasi.html",{'form':form})

@csrf_exempt
def addSubscriber(request):
	if request.method == 'POST':
		enctype= "multipart/form-data"
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']

		subscriber = Subscriber(nama = nama, email = email, password = password)
		subscriber.save()

		data = model_to_dict(subscriber)

		return HttpResponse(data)
	else:
		sub = Subscriber.objects.all().values() 
		sub_list = list(sub)  
		return JsonResponse(sub_list, safe=False)


		
@csrf_exempt
def validate_email(request):
	email_validate_check = False
	enctype = "multipart/form-data"
	if request.method == "POST":
		email = request.POST.get('email')


		if(len(email))>=1:
			email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$",email))
		


		data = {'is_taken':Subscriber.objects.filter(email = email).exists(),'is_email_valid':email_validate_check}
		return JsonResponse(data)
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")


def model_to_dict(obj):
	data = serializers.serialize('json',[obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data

@csrf_exempt
def unsub(request):
	if request.method == "POST":
		subId = request.POST['id']
		Subscriber.objects.filter(id=subId).delete()
		return HttpResponse(json.dumps({'message': "Succeed"}),content_type="application/json")
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")
