from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class lab10test(TestCase):
	def test_url_profile_is_exist(self):
		response = Client().get('/Lab10/')
		self.assertEqual(response.status_code,200)

	def test_url_regist_is_exist(self):
		response = Client().get('/Lab10/registrasi/')
		self.assertEqual(response.status_code,200)


	def test_profile_using_profile_template(self):
		response = Client().get('/Lab10/')
		self.assertTemplateUsed(response,'profile.html')

	def test_registrasi_using_regis_template(self):
		response = Client().get('/Lab10/registrasi/')
		self.assertTemplateUsed(response,'registrasi.html')

	def test_using_profile_func(self):
		found = resolve('/Lab10/')
		self.assertEqual(found.func,profile)

	def test_using_regist_func(self):
		found = resolve('/Lab10/registrasi/')
		self.assertEqual(found.func,regist)

	def test_model_can_add_subscriber(self):
		add_subscriber = Subscriber.objects.create(nama = "john",email = "john@yahoo.com", password = "johnjohn")
		count_obj = Subscriber.objects.all().count()
		self.assertEqual(count_obj,1)

	def test_add_subscriber(self):
		response = Client().post('/Lab10/registrasi/',{'nama':"nama",'email':"email",'password':"password"})
		self.assertEqual(response.status_code,200)

	def test_check_email(self):
		response = Client().post('/Lab10/validate_email/',{'nama':"nama",'email':'email'})
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code,200)
		self.assertJSONEqual(html_response,{'is_taken':False,'is_email_valid':False})

	def test_unsub(self):
		response= Client().post('/Lab10/hapusSub/',{'id':30})
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code,200)
		self.assertJSONEqual(html_response,{'message':'Succeed'})

	





# Create your tests here.
