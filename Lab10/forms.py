from django import forms
from django.forms import ModelForm
from .models import Subscriber

class subs(forms.ModelForm):
	nama = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'id':'post-text1', 'required':True,}), label='Full Name', max_length=50)
     
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control', 'id':'post-text2', 'required':True,}), label='E-mail')
    
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','id':'post-text3','required':True,}), min_length=8)
    
	class Meta:
		model =	Subscriber
		fields = ('nama', 'email', 'password',)

        

	