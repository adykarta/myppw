from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import addStatus
from .views import cekProfil
from .models import Status
from.forms import add_status
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options



class story6Test(TestCase):
	def test_Lab6_url_is_exist(self):
		response = Client().get('/Lab6/cekProfil/')
		self.assertEqual(response.status_code,200)

	def test_Story6_url_is_exist(self):
    		response = Client().get('/Lab6/')
    		self.assertEqual(response.status_code,200)
   
	def test_Lab6_using_index_template(self):
    		response = Client().get('/Lab6/cekProfil/')
    		self.assertTemplateUsed(response,'profilKu.html')
	
	def test_Story6_using_index_template(self):
        	response = Client().get('/Lab6/')
        	self.assertTemplateUsed(response, 'index.html')
 
	def test_Story6_using_addStatus_func(self):
        	found = resolve('/Lab6/')
        	self.assertEqual(found.func,addStatus)

	def test_model_can_create_new_status(self):
		new_activity = Status.objects.create(Statusku = 'ini status')
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)
	
	
	def test_Story6_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/Lab6/', {'Statusku': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/Lab6/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

	def test_create_anything_model(self, judul='ini judul'):
    		return Status.objects.create(Statusku = judul)

class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.browser.implicitly_wait(3) 
		super(NewVisitorTest,self).setUp()

	def tearDown(self):
		self.browser.implicitly_wait(3)
		self.browser.quit()

	def test_can_start_a_list_and_retrieve_it_later(self):		
		self.browser.get('http://kartadibrata.herokuapp.com/Lab6/')
		time.sleep(5)
		self.assertIn('Status Aku',self.browser.page_source)
		isi_status = self.browser.find_element_by_id('id_Statusku')
		isi_status.send_keys('coba coba')
		submit = self.browser.find_element_by_name('isi_status')
		submit.send_keys(Keys.RETURN)
		time.sleep(2)
		delete_status = self.browser.find_element_by_name('delete_status')
		delete_status.click()
		time.sleep(3)


	def test_body_color(self):
		self.browser.get('http://kartadibrata.herokuapp.com/Lab6/')
		bodycolor = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
		self.assertEqual(bodycolor,'rgba(211, 211, 211, 1)')

	def test_body_font(self):
		self.browser.get('http://kartadibrata.herokuapp.com/Lab6/')
		font = self.browser.find_element_by_tag_name('body').value_of_css_property('font-family')
		self.assertEqual(font,'helvetica')

	def test_textbox_position(self):
		self.browser.get('http://kartadibrata.herokuapp.com/Lab6/')
		statusbox = self.browser.find_element_by_id('id_Statusku')
		self.assertEqual(statusbox.location['x'], 15)
		self.assertEqual(statusbox.location['y'],88)

	def test_button_position(self):
		self.browser.get('http://kartadibrata.herokuapp.com/Lab6/')
		tambahstatus = self.browser.find_element_by_name('isi_status')
		self.assertEqual(tambahstatus.location['x'],15)
		self.assertEqual(tambahstatus.location['y'],150)

		







# if __name__ == '__main__':
# 	unittest.main(warnings='ignore')




    

# Create your tests here.
