from django.contrib import admin
from .models import Status

# Register your models here.
class StatusAdmin(admin.ModelAdmin):
	list_display = ['Statusku', 'current_time']
admin.site.register(Status,StatusAdmin)
# Register your models here.
