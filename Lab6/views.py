from django.shortcuts import render
from .models import Status
from .forms import add_status
from django.http import HttpResponse, HttpResponseRedirect

response ={}

def addStatus(request):
	form=add_status(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['Statusku'] = request.POST['Statusku']
		statusob=Status(Statusku=response['Statusku'])
		statusob.save()
		return HttpResponseRedirect('/Lab6/')
	else:
		context ={
		'stat' : Status.objects.all(),
		}

	return render(request,'index.html',{'form':add_status(), 'stat':Status.objects.all()})

def hapusStatus(request):
	Status.objects.all().delete()
	return HttpResponseRedirect('/Lab6/')

def cekProfil(request):
	return render(request,'profilKu.html')





# Create your views here.
