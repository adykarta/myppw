from django.shortcuts import render,redirect
from django.http import HttpResponse, JsonResponse
import requests
import json
from django.contrib.auth import authenticate, login
from social_django.models import *
from django.views.decorators.csrf import csrf_exempt



# Create your views here.
def data(request):
	
	try:
		q = request.GET['q']
	except:
		q = 'quilting'

	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
	jsonparse = json.dumps(getJson.json())
	return HttpResponse(jsonparse)



def index(request):
	if  request.user.is_authenticated :
		request.session['sessionkey'] = request.session.session_key
		request.session['email'] = request.user.email
		request.session['username'] = request.user.username
		request.session['fullname'] = request.user.first_name + " " + request.user.last_name
		request.session.modified = True
		
		print(request.session.items())
		


		return render(request,'buku.html')
	else:
		 return redirect ('login')
@csrf_exempt
def like(request):
	response ={}
	if(request.method=="POST"):
		counter = request.POST.get('like')
		request.session['like'] = counter
		response["message"] = counter
		return JsonResponse(response)

	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")

def login(request):
	return render(request, 'login.html')