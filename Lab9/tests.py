from django.test import TestCase
from django.http import HttpRequest, HttpResponse
from django.test import Client
from django.urls import resolve
from Lab9 import views

# Create your tests here.

class Lab9Test(TestCase):
	def test_url_like_api(self):
		response = Client().get('/Lab9/api/like/')
		self.assertEqual(response.status_code,200)

	def test_url_login(self):
		response = Client().get('/Lab9/')
		self.assertEqual(response.status_code,200)

	def test_template_login(self):
		response = Client().get('/Lab9/')
		self.assertTemplateUsed(response, 'login.html')


	def test_url_used(self):
		response = Client().get('/Lab9/buku/')
		self.assertEqual(response.status_code,302)

	# def test_template_used(self):
	# 	response = Client().get('/Lab9/buku/')
	# 	self.assertTemplateUsed(response,'buku.html')

	def test_url_for_api(self):
		response = Client().get('/Lab9/data/')
		self.assertEqual(response.status_code,200)

	def test_lab9_func(self):
		found = resolve('/Lab9/buku/')
		self.assertEqual(found.func,views.index)

	def test_like_func(self):
		found = resolve('/Lab9/api/like/')
		self.assertEqual(found.func,views.like)


	def test_lab9_json(self):
		found = resolve('/Lab9/data/')
		self.assertEqual(found.func,views.data)

	def test_login_func(self):
		found = resolve('/Lab9/')
		self.assertEqual(found.func,views.login)
