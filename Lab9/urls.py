from django.conf.urls import url,include
from Lab9 import views as Lab9
from django.contrib.auth import views
from django.conf import settings


urlpatterns = [
	url(r'^$', Lab9.login, name="login"),
	url('data',Lab9.data,name="data_books"),
	url('buku',Lab9.index, name="buku"),
	url(r'logout/', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
	url('auth/', include('social_django.urls', namespace='social')),
	url('api/like',Lab9.like, name = "like"),


]
